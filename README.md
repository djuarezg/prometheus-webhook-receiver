# Overview

This project provides the design for the automated recovery system based on alarm responses to Prometheus alert notifications.
It is based on [webhook receivers](https://prometheus.io/docs/alerting/configuration/#%3Cwebhook_config%3E) for Prometheus AlertManager. 

Actions are executed as Openshift/Kubernetes Jobs.

The receivers are capable of responding to both firing and resolved alarms, following the webhook configuration on Prometheus that indicates what receiver endpoint an alert should be send to.

# Related resources

## Docker images

The following Docker images are stored on both Gitlab's and Openshift's registry.

Gitlab CI is responsible for storing the necessary Docker images on Gitlab registry (See <https://gitlab.cern.ch/ci-tools/docker-image-builder>).
We want to [mirror images](https://docs.openshift.com/container-platform/3.11/dev_guide/managing_images.html#managing-images-mirror-registry-images) from Gitlab registry to Openshift's internal registry (via imagestreams);
this allows to use the appropriate Docker images even if Gitlab's registry is inaccessible. This approach requires that target applications's CI mirrors the webhook receiver and its alarm-response images on its Openshift namespace.

## [Prometheus webhook receiver](https://gitlab.cern.ch/paas-tools/monitoring/prometheus-webhook-receiver)

This component receives the Prometheus alarms, either when an alert is fired or when it is resolved.

If the alert annotations contain a YAML Job definition corresponding to the event (firing/resolved), the Job
is created in the receiver's local namespace. 

This means we may run several instances of the receiver, one per namespace where response jobs need to be run.

The action to execute in case of alert is completely defined in the alert, the receiver only provides a generic
Job execution service and does not need any knowledge of what alerts it receives.

## [Prometheus responses base image](https://gitlab.cern.ch/paas-tools/prometheus-responses-base)

This project is a base image with the basic tools needed to run the alarm responses. This is the baseline for any custom responses image.

Any extra tools must be added to this project if they are a common feature, otherwise use the specific projects to extend the base image.

Please check [the corresponding project](https://gitlab.cern.ch/paas-tools/prometheus-responses-base) to see the complete list of tools.

## [Gitlab Prometheus responses image](https://gitlab.cern.ch/vcs/prometheus-responses-gitlab)

This project extends [Prometheus responses base image](https://gitlab.cern.ch/paas-tools/prometheus-responses-base) with response alarms related to Gitlab and any specific tools necessary for them.

## [Openshift infrastructure Prometheus responses image](https://gitlab.cern.ch/paas-tools/monitoring/prometheus-responses-paas)

This project extends [Prometheus responses base image](https://gitlab.cern.ch/paas-tools/prometheus-responses-base) with response alarms related to Openshift infrastructure (PaaS Infra) and any specific tools necessary for them.

## Openshift resources

### Deployments

A webhook receiver is deployed for each of the following deployments. These can be deployed with the included Helm chart.

#### Gitlab

A webhook receiver instance is deployed on `gitlab` namespace on both `openshift` and `openshift-dev`.

[oo-gitlab-cern](https://gitlab.cern.ch/vcs/oo-gitlab-cern) is responsible for the deployment.

This receiver deployment is in charge of responding to Gitlab's alarms.

#### Paas-infra

A webhook receiver instance is deployed on `paas-infra` namespace on both `openshift` and `openshift-dev`.

[prometheus-webhook-receiver deployer](https://gitlab.cern.ch/paas-tools/paas-infra/prometheus-webhook-receiver) is responsible for the deployment.

This receiver deployment is in charge of responding to Openshift infraestructure alarms.

### Response secrets

If needed, job definitions can use pre-existing secrets or configmaps set up by the target application.
Any permission for the Job should be given by the serviceaccount it uses.

### Service accounts

The webhook receiver Docker image needs to run with a service account capable of:

* Reading configMaps from the Prometheus namespace, see [./example_resources/prometheus-receiver-rbac-template.yaml](./example_resources/prometheus-receiver-rbac-template.yaml) for the corresponding role.
    This allows to retrieve the YAML job definitions from the same namespace the Prometheus instance is running on.
* Creating Openshift jobs on the target application, see [./example_resources/target-receiver-rbac-template.yaml](./example_resources/target-receiver-rbac-template.yaml) for the corresponding role and rolebindings.
    In order to create the corresponding response with the previous job definition on the target application namespace.

The following diagram represents how roles, service accounts and rolebindings are spread across namespaces:
```plantuml
package "Prometheus deployment" {
  (Role to read job definitions configMap) <--> (RoleBinding to read configMap)
  database "Job definitions configMap" as cm
}
package "Target application" {
  [Webhook receiver] --> (Receiver Service account) : Uses
  (Role to create jobs) <--> (RoleBinding to create jobs)
  (RoleBinding to create jobs) <--> (Receiver Service account)
  (RoleBinding to read configMap) <--> (Receiver Service account)
}
```

* The service account belongs to the target application namespace, as well as the role and rolebinding.
* While the configMap reading role and its binding belong to the Prometheus instance namespace, the RoleBinding is related to the service account on the target namespace.

**NB: This service account is the one used to run the receiver, not the one used for the jobs. Job definitions need to run as a service account which will need to be granted the appropriate permissions in the target namespace to perform its tasks.**

# Webhook receiver interaction

The following diagram represents how all components interact with each other:

```plantuml
package "Prometheus deployment" {
  (Target metrics) ..> [Prometheus instance]
  [Prometheus instance] ..> [Alertmanager] : Alert
  database "Job definitions configMap" as cm
}
package "Target application" {
  [Alertmanager] -> [Webhook receiver] : JSON Alert definition

  [Webhook receiver] ..> (Openshift Job) : Create job with CM definition
  [Webhook receiver] ..> cm : Read job  definition matching Alert key
}
```

The complete process is as follows:

* Prometheus configured alert rules define when to notify an alarm. This are stored on the Prometheus namespace in a configMap.
* Prometheus notifies the Alertmanager whenever an alarm meets the rule conditions.
* Alertmanager decides to which webhook receiver it sends the alarm depending on its [routes](https://prometheus.io/docs/alerting/configuration/#%3Croute%3E)
* Alert manager sends the notification to the configured receivers, i.e. email and webhook receivers. This sends the alarm to the receiver on the target application namespace.
* The Prometheus webhook receiver gets the JSON definition of the alert including the keys to retrieve the corresponding `firing_job` or `resolved_job` YAML job definition from `receiver-job-definitions` configMap.
* The webhook receiver creates a job following this previous definition on the current Openshift namespace where the receiver is running.
* Since the Prometheus webhook receiver and the target application will be on different namespaces, [networks have to be joined](https://docs.openshift.com/container-platform/3.10/admin_guide/managing_networking.html#joining-project-networks).

# Naming conventions

## Alarm definitions

**Alarm YAML definitions are stored on a configMap on the target namespace named `receiver-job-definitions`. Each key corresponds to a job definition used as a response.**

In order to respond to a Prometheus alert, the alert definition needs to have the following annotations:

`firing_job`: This annotation corresponds to the key on the `receiver-job-definitions` configMap with the job response to run when the Prometheus rule fires an alert.
`resolved_job`: This annotation corresponds to the key on the `receiver-job-definitions` configMap with the job response to run when the Prometheus rule sends the resolution to an alert.

This is an example alarm:
```bash

  - alert: gitlab_elasticsearch_cluster_not_healthy
    expr: elasticsearch_cluster_health_status{kubernetes_namespace="gitlab",color="red"} == 1 OR absent(elasticsearch_cluster_health_status{kubernetes_namespace="gitlab"})
    for: 10m
    labels:
      severity: important
    annotations:
      firing_job: disable_global_search
      resolved_job: enable_global_search
```

## Job naming conventions

Job definitions are stored on the `receiver-job-definitions` configMap as YAML. 

They need a generate name property indicating whether it is a firing or resolved alarm, as well as refering to the alarm name, such as in this example:

```bash
apiVersion: batch/v1
kind: Job
metadata:
  generateName: firing-gitlab-elasticsearch-cluster-not-healthy-response-
  labels:
    app: prometheus-webhook-receiver
spec:
  parallelism: 1
  completions: 1
  template:
      labels:
        app: prometheus-webhook-receiver
      spec:
        containers:
        - name: ansible-job
          image: gitlab-registry.cern.ch/paas-tools/monitoring/prometheus-responses:latest
          args:
          - bash
          - -c
          - |-
            ansible-playbook gitlab_elasticsearch_cluster_not_healthy.yml
          envFrom:
          - configMapRef:
              name: prometheus-webhook-job-secrets
        imagePullPolicy: Always
        restartPolicy: Never
        serviceAccount: <desired-sa>
        serviceAccountName: <desired-sa>
# OR

apiVersion: batch/v1
kind: Job
metadata:
  generateName: resolved-gitlab-elasticsearch-cluster-not-healthy-response-
  labels:
    app: prometheus-webhook-receiver
spec:
  parallelism: 1
  completions: 1
  template:
      metadata:
        generateName: resolved-gitlab-elasticsearch-cluster-not-healthy-response-
      labels:
        app: prometheus-webhook-receiver
      spec:
        containers:
        - name: ansible-job
          image: gitlab-registry.cern.ch/paas-tools/monitoring/prometheus-responses:latest
          args:
          - bash
          - -c
          - |-
            ansible-playbook gitlab_elasticsearch_cluster_healthy.yml
          envFrom:
          - configMapRef:
              name: prometheus-webhook-job-secrets
        imagePullPolicy: Always
        restartPolicy: Never
        serviceAccount: <desired-sa>
        serviceAccountName: <desired-sa>
```

i.e. generateName must be `firing-alarm-name-response-` or `resolved-alarm-name-response-`.

Job labels must be at least:

```bash
labels:
    app: prometheus-webhook-receiver
```

The Job itself can be defined as desired, as long as you follow the established conventions to avoid maintenance problems.

**NB: Keep in mind that Openshift naming convention for objects it a DNS-1123 subdomain which consists of lower case alphanumeric characters, '-' or '.', and must start and end with an alphanumeric character.**

# Resources included on this project

* A Helm chart deployment. Please check [the required steps for further information](./prometheus-webhook-receiver.md)
  * It includes the required ServiceAccounts, Roles and Rolebindings for the deployment to work.
