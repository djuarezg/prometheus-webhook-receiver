FROM scratch 
# Add a previously built app binary to an empty image
# To build the binary:
COPY main  /
# /main <job_destination_namespace> <job_image> <secrets_configmap> <service_account>
CMD ["/main"]
